; -*- coding: utf-8 -*-
;
;Z SD_ACCEPT  addr addr len -- addr' len'  get line to interpret from a SD Card file
; no interrupt allowed
; defered word ACCEPT is redirected here by the word LOAD"
; "defered" word CIB is redirected to SDIB (PAD if RAM<2k) by the word LOAD"
; sequentially move an input line ended by CRLF from SD_BUF to PAD
;   if end of SD_BUF is reached before CRLF, asks Read_File to refill buffer with next sector
;   then load the end of the line to SDIB_ptr.
; when the last buffer is loaded, the handle is automaticaly closed
; when all LOAD"ed files are read, redirects defered word ACCEPT to default ACCEPT and restore interpret pointers.
; see CloseHandle.

; used variables : BufferPtr, BufferLen
; YEMIT uses only IP and Y registers
; ==================================;
;    FORTHWORD "SD_ACCEPT"          ; SDIB_org SDIB_org SDIB_len -- SDIB_org len    IP = QUIT4
; ==================================;
SD_ACCEPT                           ; sequentially move from SD_BUF to SDIB a line of chars delimited by CRLF
        PUSH    IP                  ;                           R-- IP
        MOV     #SD_YEMIT_RET,IP    ; set YEMIT return
; ----------------------------------;
        ADD     TOS,0(PSP)          ; -- DstOrg MaxEnd DstLen
        MOV     2(PSP),TOS          ; -- DstOrg MaxEnd DstPtr
        JMP     SDA_InitSrcAddr     ;
; ----------------------------------;
SDA_GetFileNextSect                 ; -- DstOrg MaxEnd DstPtr
; ----------------------------------;
        CALL    #Read_File          ;
; ==================================;
SDA_InitSrcAddr                     ; -- DstOrg MaxEnd DstPtr
; ==================================;
        MOV     &BufferPtr,S        ;
        MOV     &BufferLen,T        ;
        JMP     SDA_ComputeCharLoop ;
; ----------------------------------;
SD_YEMIT_RET                        ;
; ----------------------------------;
        mHI2LO                      ;
        SUB     #2,IP               ; 1                     restore YEMIT return
; ----------------------------------;
SDA_ComputeCharLoop                 ; -- DstOrg MaxEnd DstPtr
; ----------------------------------;
        CMP     T,S                 ; 1                     SrcPtr >= SrcLen ?
        JC      SDA_GetFileNextSect ; 2                     if yes
        MOV.B   SD_BUF(S),Y         ; 3                     Y = char
        ADD     #1,S                ; 1                     increment SrcPtr
        CMP.B   #10,Y               ; 2                     control char = 'LF' ?
        JZ      SDA_EndOfLine       ; 2                     yes
        CMP.B   #32,Y               ; 2                     control char < 'BL' ?
        JNC     SDA_ComputeCharLoop ; 2                     yes loop back
;        CMP.B   #13,Y               ; 2                     control char = 'CR' ?
;        JZ      SDA_ComputeCharLoop ; 2                     yes loop back
        CMP     @PSP,TOS            ; 2                     ptr = end ?
        JZ      YEMIT               ; 2                     yes, don't move char to dst
        MOV.B   Y,0(TOS)            ; 3                     move char to dst
        ADD     #1,TOS              ; 1                     increment DstPtr
        JMP     YEMIT               ; 9/6~                  send echo to terminal if ECHO, do nothing if NOECHO
; ----------------------------------;
SDA_EndOfLine                       ; -- DstOrg MaxEnd DstEnd
; ----------------------------------;
        MOV     S,&BufferPtr        ;                       save SD_buf_ptr for next line loop (next SD_ACCEPT)
        MOV     &CurrentHdl,T       ;
        MOV     S,HDLW_BUFofst(T)   ;                       update handle
        ADD     #2,PSP              ; -- DstOrg DstEnd
        SUB     @PSP,TOS            ; -- ORG LEN
; ==================================;
SDA_EndOfFile                       ; -- PrvOrg PrvLen      <== CloseHandle return, with the remainder of line containing the LOAD" command
; ==================================;                       R-- INTERPRET
        MOV     @RSP+,IP            ;                       R--
        MOV     @IP+,PC             ;
; ----------------------------------;
