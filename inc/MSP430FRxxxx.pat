
@set-syntax{C;\;}!  tell GEMA to replace default Comment separator '!' by ';'

;MSP430FRxxxx.pat

; ============================================
; FAST FORTH V4.2
; ============================================

; ============================================
; FRAM INFO
; ============================================
INFO_ORG=\$1800;
INFO_LEN=\$0200;

; You can check the addresses below by comparing their values in DTCforthMSP430FRxxxx.lst
; those addresses are usable with the symbolic assembler

FREQ_KHZ=\$1800;        FREQUENCY (in kHz)
TERMBRW_RST=\$1802;     TERMBRW_RST
TERMMCTLW_RST=\$1804;   TERMMCTLW_RST
I2CBASEADR=\$1802;      I2C_SLAVE base address
I2CSLAVEADR=\$1804;     I2C_SLAVE address 
USERSYS=\$1806;         user SYS variable, defines software RESET, DEEP_RST, INIT_HARWARE, etc.
VERSION=\$1808;
THREADS=\$180A;         THREADS
; ---------------------------------------------
KERNEL_ADDON=\$180C;
; ---------------------------------------------
FLOORED_DIV=\$8000;     BIT15=FLOORED DIVISION
LF_XTAL=\$4000;         BIT14=LF_XTAL
;                       BIT13=UART CTS
;                       BIT12=UART RTS
;                       BIT11=UART XON/XOFF
;                       BIT10=UART half duplex
;                       BIT9=Q15.16 input
;                       BIT8=DOUBLE input
;                       BIT7=assembler 20 bits
;                       BIT6=assembler 16 bits with 20 bits addr
HMPY=\$20;              BIT5=hardware MPY
;                       BIT4=WARM display with SYSUNIV + SYSSNIV + SYSRSTIV number
NO_VLO_TIM=8;           BIT3=LF_XTAL-->ACLK-->TIMERx_CLK flag (MSP430FR5xxx|MSP430FR6xxx)
;                       BIT2=
;                       BIT1=
;                       BIT0=
; ----------------------------------------------
DEEP_ORG=\$180E;        MOV #DEEP_ORG,X                         TERMINAL
; ----------------------------------------------             UART       I2C
DEEP_TERM_VEC=\$180E;   address of default TERMINAL vect.--> UCAx       UCBx
DEEP_STOP=\$1810;       address of default STOP_APP      --> TX_IDLE    RET
DEEP_ABORT=\$1812;      address of default ABORT_APP:    -->   ABORT_TERM
DEEP_SOFT=\$1814;       address of default SOFT_APP:     -->       RET
DEEP_HARD=\$1816;       address of default HARD_APP:     -->    INIT_TERM
DEEP_BACKGRND=\$1818;   address of default BACKGRND_APP: --> RX_ON    I2C_ACCEPT
DEEP_DP=\$181A;         to DEEP_INIT RST_DP
DEEP_LASTVOC=\$181C;    to DEEP_INIT RST_LASTVOC
DEEP_CURRENT=\$181E;    to DEEP_INIT RST_CURRENT
DEEP_CONTEXT=\$1820;    to DEEP_INIT RST_CONTEXT
;                       NULL_WORD
; ----------------------------------------------
PUC_SYS_ORG=\$1824;     MOV #PUC_SYS_ORG,X
; ----------------------------------------------
INIT_ACCEPT=\$1824;     to INIT PFA_ACCEPT  (FRAM)
INIT_EMIT=\$1826;       to INIT PFA_EMIT    (FRAM)
INIT_KEY=\$1828;        to INIT PFA_KEY     (FRAM)
INIT_CIB=\$182A;        to INIT CIB_ORG     (FRAM)
; ----------------------------------------------
FORTH_ORG=\$182C;       MOV #FORTH_ORG,X
; ----------------------------------------------
INIT_BASE=\$182C;       to INIT BASE     (RAM)
INIT_LEAVE=\$182E;      to INIT LEAVEPTR (RAM)
; ----------------------------------------------
INIT_DOXXX=\$1830;      MOV #INIT_DOXXX,X
; ----------------------------------------------
INIT_DOVAR=\$1830;      to INIT rDOVAR   (R7)
INIT_DOCON=\$1832;      to INIT rDOCON   (R6)
INIT_DODOES=\$1834;     to INIT rDODOES  (R5)
INIT_DOCOL=\$1836;      to INIT rDOCOL   (R4)
;
; ----------------------------------------------
RST_ORG=\$1838;
RST_LEN=\$14;           20 bytes
; ----------------------------------------------
STOP_APP=\$1838;        address of current STOP_APP
ABORT_APP=\$183A;       address of current ABORT_APP
SOFT_APP=\$183C;        address of current SOFT_APP
HARD_APP=\$183E;        address of current HARD_APP
BACK_APP=\$1840;        address of current BACKGRND_APP
RST_DP=\$1842;          RST_RET value for (RAM) DDP
RST_LASTVOC=\$1844;     RST_RET value for (RAM) LASTVOC
RST_CURRENT=\$1846;     RST_RET value for (RAM) CURRENT
RST_CONTEXT=\$1848;     RST_RET value for (RAM) CONTEXT (8 CELLS) + NULL_WORD
;
RAM_USR_PTR=\$185C;
INFO_USR_PTR=\$185E;
; ===============================================
; FAST FORTH V 4.0: FRAM usage, INFO space free from $1860 to $19FF
; ===============================================
INFO_USR_ORG=\$1860;
;
; ============================================
; FRAM TLV
; ============================================
TLV_ORG=\$1A00;         Device Descriptor Info (Tag-Lenght-Value)
TLV_LEN=\$0100;       
DEVICEID=\$1A04;

; ============================================
; FRAM MAIN
; ============================================
; to use in ASSEMBLER mode
;
\#XDOVAR=&\$1830;           to restore rDOVAR:  MOV #XDOVAR,rDOVAR
\#XDOCON=&\$1832;           to restore rDOCON:  MOV #XDOCON,rDOCON    
\#XDODOES=&\$1834;          to restore rDODOES: MOV #XDODOES,rDODOES
\#XDOCOL=&\$1836;           to restore rDOCOL:  MOV #XDOCOL,rDOCOL
;
\#SKIP_DOWNLOAD=\#MAIN_ORG;
\#UART_RXOFF=\#ACCEPT+\$26; UART TERMINAL: CALL #UART_RXOFF
\#UART_RXON=\#KEY+8;        UART TERMINAL: CALL #UART_RXON
\#I2C_CTRL=\#KEY+\$0A;      I2CM TERMINAL: used as is: MOV.B #<CTRL_CHAR>,Y
;                                                      CALL #I2C_CTRL
\#LIT=\#TYPE+\$1C;          asm CODE run time of LITERAL
\#XSQUOTE=\#TYPE+\$30;      asm CODE run time of QUOTE     
\#SETIB=\#TYPE+\$44;        FORTH CODE Set Input Buffer with org & len values, reset >IN pointer 
\#REFILL=\#TYPE+\$54;       FORTH CODE accept one line from input and leave org len of input buffer
\#QFBRAN=\#TYPE+\$6C;       FORTH CODE compiled by IF UNTIL
\#ZBRAN=\#TYPE+\$70;
\#BRAN=\#TYPE+\$72;         FORTH CODE compiled by ELSE REPEAT AGAIN
\#CODE_NEXT=\#[THEN];       FORTH CODE NEXT instruction (MOV @IP+,PC)
CODE_DEFER=MOV \#[THEN],R0; CODE <name>
;                           CODE_DEFER
;                           END_CODE
\#MUSMOD=\#\<\#+8;          asm CODE 32/16 unsigned division, used by ?NUMBER, UM/MOD
\#MDIV1DIV2=\#\<\#+\$1A;    asm CODE input for 48/16 unsigned division with DVDhi=0, see DOUBLE M*/
\#MDIV1=\#\<\#+\$22;        asm CODE input for 48/16 unsigned division, see DOUBLE M*/
\#RET_ADR=\#\<\#+\$4C;      asm CODE            used by MARKER
\#UDOTNEXT=\#U\.+\$0A;      MOV #UDOTNEXT,PC          used in DOUBLE.f
\#BL_WORD=\#WORD+2;
\#INTERPRET=\#LITERAL-\$4E; MOV #INTERPRET,PC   used in CORE_ANS.f
\#EXECUTE=\#LITERAL-\$2E;   MOV #EXECUTE,PC     used in CORE_ANS.f
\#ABORT_DOWNLOAD=\#ALLOT+8; then ABORT
\#ABORT=\#ALLOT+\$0C;       MOV #ABORT,PC       used in CORE_ANS.f
\#QUIT=\#ALLOT+\$12;        MOV #QUIT,PC        used in CORE_ANS.f
\#MVT_WRD_X2W=\#RST_RET-\$16; to MOV T words from @X+ to 0(W)+
\#SYS_NO_STOP=\#SYS+4;      SYS without STOP_U2I
\#WARM=&SYS+\$16;           MOV #WARM,0(RSP)
\#COLD=&\$FFFA;             vector value by default
\#Read_File=&READ+\$0C;     CALL #Read_File, sequentially load a sector in SD_BUF
\#Write_File=\#WRITE+\$4;   CALL #Write_File, sequentially write SD_BUF in a sector

; to use in FORTH compilation mode
;
;INTERPRET=\[ \' LITERAL \$4E - \, \];' 
;EXECUTE=\[ \' LITERAL \$2E - \, \];  '
;BL_WORD=\[ \' WORD 2 + \, \];       ' ;         
WORD_CAPS_OFF=\[ \' WORD \$10 + \, \]; ' ;use: CODE MOVE #0,T COLON WORDS_CAPS_OFF ... ;

; ============================================
; UARTI2CS APPLICATION
; ============================================
U2I_ENV_LEN=\$12;
&PRV_STOP_U2I=&_UART2I2CM_;         <-- STOP-4TH
&PRV_ABORT_U2I=&_UART2I2CM_+2;      <-- ABORT_4TH
&PRV_SOFT_U2I=&_UART2I2CM_+4;       <-- SOFT_4TH
&PRV_HARD_U2I=&_UART2I2CM_+6;       <-- HARD_4TH
&PRV_BACK_U2I=&_UART2I2CM_+8;       <-- BACK_4TH
&PRV_TICK_VEC=&_UART2I2CM_+\$0A;    <-- INT_IN_VEC
&PRV_TERM_VEC=&_UART2I2CM_+\$0C;    <-- TERM_VEC
&I2C_SLV_ADR=&_UART2I2CM_+\$0E;     <-- I2C_Slave address
&COLLISION_DLY=&_UART2I2CM_+\$10;   <-- 20 us resolution delay after I2C collision
&DUPLEX_MODE=&_UART2I2CM_+\$11;     <-- flag = 4 --> NOECHO, <> 4 --> ECHO, -1 = I2C link lost

; I2C_MM variables for I2CMM
&I2CMM_ADR=-2(S);
&I2CMM_CNT=-1(S); 


;=========================
; TOS value for SYS events
;=========================
;
; $02 <= TOS (even) < $40 --> SYSTEM (BOR|PUC) failures: RESET --> WARM
; ---------------------------------------------------------------------

; see MSP430FR2xxx.pat|MSP430FR5xxx.pat|MSP430FR57xx.pat. 


; $40 <= TOS (even) < $60 --> USER BOR failures: SYS --> RESET --> WARM
; ---------------------------------------------------------------------
BREAK_EVENT=\#64;    teraterm(ALT+B) request


; $60 <= TOS (even) < $80 --> USER BOR events: SYS --> RESET --> WARM
; -------------------------------------------------------------------
USER_EVENTS=\#96;
QUIT_EVENT=\#96;

; $60 < TOS (odd) < $80    USER WARM #events: SYS --> WARM
; --------------------------------------------------------
I2CMM_COLLISION=\#97;
I2CMM_NACK_ADR=\#99;
I2CMM_NACK_DATA=\#101;

; $80 <= TOS (even) < $100 --> USER SWBOR events: SYS --> RESET --> DEEP_RESET --> WARM
; -------------------------------------------------------------------------


; $01 <= TOS (odd) < $100 --> USER events: SYS --> WARM
; -----------------------------------------------------
