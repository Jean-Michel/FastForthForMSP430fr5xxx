; SelTarget.inc

    .IFDEF MSP_EXP430FR5739
NO_VLO_TIM
LF_XTAL
        .IFDEF UART_TERMINAL
UCA0_TERM
            .include "UART_BAUDRATE.inc"
        .ELSE
UCB0_TERM
        .ENDIF
        .include "MSP430FR5739.inc"
    .ENDIF
    .IFDEF MSP_EXP430FR5969
; ===================================
NO_VLO_TIM
LF_XTAL
        .IFDEF UART_TERMINAL
UCA0_TERM
            .include "UART_BAUDRATE.inc"
        .ELSE
UCB0_TERM
        .ENDIF
UCA1_SD
        .include "MSP430FR5969.inc"
    .ENDIF
    .IFDEF MSP_EXP430FR5994
; ===================================
NO_VLO_TIM
LF_XTAL
        .IFDEF UART_TERMINAL
UCA0_TERM
            .include "UART_BAUDRATE.inc"
        .ELSE
UCB2_TERM
        .ENDIF
UCB0_SD
        .include "MSP430FR5994.inc"
    .ENDIF
    .IFDEF MSP_EXP430FR6989
; ===================================
NO_VLO_TIM
LF_XTAL
        .IFDEF UART_TERMINAL
UCA1_TERM
            .include "UART_BAUDRATE.inc"
        .ELSE
UCB0_TERM
        .ENDIF
UCA0_SD
        .INCLUDE "MSP430FR6989.inc"
    .ENDIF
    .IFDEF MSP_EXP430FR4133
; ===================================
LF_XTAL
        .IFDEF UART_TERMINAL
UCA0_TERM
UCB0_SD
            .include "UART_BAUDRATE.inc"
        .ELSE
UCB0_TERM
UCA0_SD
        .ENDIF
        .INCLUDE "MSP430FR4133.inc"
    .ENDIF
    .IFDEF MSP_EXP430FR2433
; ===================================
LF_XTAL
        .IFDEF UART_TERMINAL
UCA0_TERM
            .include "UART_BAUDRATE.inc"
        .ELSE
UCB0_TERM
        .ENDIF
UCA1_SD
    .include "MSP430FR2433.inc"
    .ENDIF
    .IFDEF CHIPSTICK_FR2433
; ===================================
        .IFDEF UART_TERMINAL
UCA0_TERM
UCB0_SD
            .include "UART_BAUDRATE.inc"
        .ELSE
UCB0_TERM
UCA0_SD
        .ENDIF
    .include "MSP430FR2433.inc"
    .ENDIF
    .IFDEF MSP_EXP430FR2355
; ===================================
LF_XTAL
        .IFDEF UART_TERMINAL
UCA1_TERM
HANDSHAKOUT .equ    P2OUT
HANDSHAKIN  .equ    P2IN
RTS         .equ    1           ; P2.0
CTS         .equ    2           ; P2.1
            .include "UART_BAUDRATE.inc"
        .ELSE
UCB0_TERM
; uncomment to define some I2C_addr dedicated pins
I2C_BASE_ADR    .set 020h   ; tell INIT_TERM compiling to compute I2C_Address from dedicated pins inputs :
I2CADR_BUS      .equ 0Fh    ;
PI2CADR_IN      .equ 240h   ; P5IN
PI2CADR_OUT     .equ 242h   ; P5OUT
        .ENDIF
UCB1_SD
    .include "MSP430FR2355.inc"
    .ENDIF
    .IFDEF LP_MSP430FR2476
; ===================================
; LF_XTAL       ; connect resistors R2=0k, R3=0k before uncomment this line
        .IFDEF UART_TERMINAL
UCA0_TERM
            .include "UART_BAUDRATE.inc"
        .ELSE
UCB1_TERM
        .ENDIF
UCA1_SD
        .include "MSP430FR2476.inc"
    .ENDIF
    .IFDEF YOUR_TARGET
; ===================================
; YOUR_TARGET I/O, MEMORY, SFR, vectors as minimum FORTH I/O declarations
; vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
; add here your device.inc item:
;^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    .ENDIF
    .IFDEF MY_MSP430FR5738
; ===================================
NO_VLO_TIM
LF_XTAL
        .IFDEF UART_TERMINAL
UCA0_TERM
            .include "UART_BAUDRATE.inc"
        .ELSE
UCB0_TERM
        .ENDIF
        .include "MSP430FR5738.inc"
    .ENDIF
    .IFDEF MY_MSP430FR5738_1
; ===================================
NO_VLO_TIM
LF_XTAL
        .IFDEF UART_TERMINAL
UCA0_TERM
            .include "UART_BAUDRATE.inc"
        .ELSE
UCB0_TERM
        .ENDIF
        .include "MSP430FR5738.inc"
    .ENDIF
    .IFDEF MY_MSP430FR5738_2
; ===================================
NO_VLO_TIM
LF_XTAL
        .IFDEF UART_TERMINAL
UCA0_TERM
            .include "UART_BAUDRATE.inc"
        .ELSE
UCB0_TERM
        .ENDIF
        .include "MSP430FR5738.inc"
    .ENDIF
    .IFDEF MY_MSP430FR5948
; ===================================
NO_VLO_TIM
LF_XTAL
        .IFDEF UART_TERMINAL
UCA0_TERM
            .include "UART_BAUDRATE.inc"
        .ELSE
UCB0_TERM
        .ENDIF
UCA1_SD
        .include "MSP430FR5948.inc"
    .ENDIF
    .IFDEF JMJ_BOX_2018_10_29
; ===================================
NO_VLO_TIM
NO_LEDS
NO_SWITCHES
        .IFDEF UART_TERMINAL
UCA0_TERM
            .include "UART_BAUDRATE.inc"
        .ENDIF
        .include "MSP430FR5738.inc"
    .ENDIF
    .IFDEF JMJ_BOX_2355_2024_03_21
; ===================================
NO_LEDS
NO_SWITCHES
UCA1_TERM
        .include "UART_BAUDRATE.inc"
HANDSHAKOUT .equ    P4OUT
HANDSHAKIN  .equ    P4IN
RTS         .equ    40h           ; P4.6
CTS         .equ    80h           ; P4.7
        .include "MSP430FR2355.inc"
    .ENDIF
    .IFDEF JMJ_BOX_5738_2024_02_21
; ===================================
NO_VLO_TIM
NO_LEDS
NO_SWITCHES
UCA0_TERM
        .include "UART_BAUDRATE.inc"
        .include "MSP430FR5738.inc"
    .ENDIF
    .IFDEF FP_MAIN_2021_04_09
; ===================================
NO_LEDS
NO_SWITCHES
UCA1_TERM
; 20      --- P4.0 --> PWR_ON ----------> 3 ISO7041 as CTS high
; 19      --- P4.1 --> RTS  ------------> 4 ISO7041 
; 18      --- P4.2 <-- RX1  <------------ 6 ISO7041 
; 17      --- P4.3 --> TX1  ------------> 5 ISO7041
; RTS output is wired to the CTS input of UART2USB bridge
; configure RTS as output high (false) to disable RX TERM during start FORTH

HANDSHAKOUT .equ    P4OUT
RTS         .equ    2
        .include "UART_BAUDRATE.inc"
        .include "MSP430FR2355.inc"
    .ENDIF
    .IFDEF FP_MAIN_2024_10_14
; ===================================
NO_LEDS
NO_SWITCHES
; 20 --- P4.0 <-- CTS      <---------- U4.6 ISO7021 (option) 
; 19 --- P4.1 --> RTS      ----------> U4.7 ISO7021 (option) 
UCA1_TERM
HANDSHAKIN  .equ    P4IN
HANDSHAKOUT .equ    P4OUT
CTS         .equ    1
RTS         .equ    2
        .include "UART_BAUDRATE.inc"
        .include "MSP430FR2355.inc"
    .ENDIF
    .IFDEF FP_XLR_2021_05_04
; ===================================
NO_LEDS
NO_SWITCHES
UCB1_TERM

; P1.0  --------  2 <---------- I2CADR0
; P1.1  --------  1 <---------- I2CADR1
; P1.2  -------- 40 <---------- I2CADR2
; P1.3  -------- 39 <---------- I2CADR3
I2C_BASE_ADR    .set 020h   ; tell INIT_TERM compiling to compute I2C_Address from dedicated pins inputs :
I2CADR_BUS      .equ 0Fh    ;
PI2CADR_IN      .equ 200h   ; P1IN
PI2CADR_OUT     .equ 202h   ; P1OUT
        .include "UART_BAUDRATE.inc"
        .include "MSP430FR2355.inc"
    .ENDIF
    .IFDEF FP_XLR_2024_10_14
; ===================================
NO_LEDS
NO_SWITCHES
        .IFNDEF UART_TERMINAL
UCB1_TERM
; P1.0  --------  2 <---------- I2CADR0
; P1.1  --------  1 <---------- I2CADR1
; P1.2  -------- 40 <---------- I2CADR2
; P1.3  -------- 39 <---------- I2CADR3
I2C_BASE_ADR    .set 020h   ; tell INIT_TERM compiling to compute I2C_Address from dedicated pins inputs :
I2CADR_BUS      .equ 0Fh    ;
PI2CADR_IN      .equ 200h   ; P1IN
PI2CADR_OUT     .equ 202h   ; P1OUT
        .ENDIF
        .include "MSP430FR2355.inc"
    .ENDIF
