; -*- coding: utf-8 -*-
;
; ---------------------------------------
; UART TERMINAL driver for FastForth target
; ---------------------------------------
;
;                     +---------------------------------------+
;                     |    +-----------------------------+    |
;                     |    |    +------(option)-----+    |    |
;                     |    |    |                   |    |    |
; FastForth target:  TXD  RXD  RTS  connected to : CTS  TXD  RXD of UARTtoUSB <--> COMx <--> TERMINAL
; ----------------   ---  ---  ---                 ---  ---  ---    -------------------------------------
; MSP_EXP430FR5739   P2.0 P2.1 P2.2                                 PL2303TA                 TERATERM.EXE
; MSP_EXP430FR5969   P2.0 P2.1 P4.1                                 PL2303HXD/GC
; MSP_EXP430FR5994   P2.0 P2.1 P4.2                                 CP2102
; MSP_EXP430FR6989   P3.4 P3.5 P3.0
; MSP_EXP430FR4133   P1.0 P1.1 P2.3
; CHIPSTICK_FR2433   P1.4 P1.5 P3.2
; MSP_EXP430FR2433   P1.4 P1.5 P1.0
; MSP_EXP430FR2355   P4.3 P4.2 P2.0
; LP_MSP430FR2476    P1.4 P1.5 P6.1
;
;------------------------------------------------------------------------------------------------------------
; UART TERMINAL: QABORT, INIT values of ABORT_APP, BACKGRND_APP, HARD_APP, COLD_APP and SOFT_APP, WARM
;------------------------------------------------------------------------------------------------------------

; ==================================;
SKIP_DOWNLOAD                       ;
; ==================================;
            CALL #UART_RXON         ;
A_UART_LOOP BIC #RX_TERM,&TERM_IFG  ; clear RX_TERM
            MOV &FREQ_KHZ,Y         ; 1000, 2000, 4000, 8000, 16000, 24000
A_USB_LOOPJ MOV #65,X               ; 2~        <-------+ linux with minicom seems very very slow...
A_USB_LOOPI SUB #1,X                ; 1~        <---+   | ...to refill its USB buffer ==> set 200 ms...
            JNZ A_USB_LOOPI         ; 2~ 3~ loop ---+   |  ==> ((65*3)+5)*1000 = 200ms delay
            SUB #1,Y                ; 1~                |
            JNZ A_USB_LOOPJ         ; 2~ 200~ loop -----+
            BIT #RX_TERM,&TERM_IFG  ; test RX_TERM INT
            JNZ A_UART_LOOP         ; if new char in TERMRXBUF after 200ms delay
SKIPDWN_END MOV @RSP+,PC            ; Y = X = 0
; ----------------------------------;

; ?ABORT defines the run-time part of ABORT"
;-----------------------------------;
QABORT      CMP #0,2(PSP)           ; -- f addr cnt     if f is true abort current process then display ABORT" msg.
            JNZ QABORT_YES          ;
            ADD #4,PSP              ; -- cnt
            JMP DROP                ;
; ----------------------------------;
QABORT_YES                          ; -- f addr cnt     QABORT_YES called by INTERPRET, QREVEAL, TYPE2DOES
; ----------------------------------;
            SUB #2,PSP              ;              
            MOV TOS,0(PSP)          ;
            MOV &PSTACK-2,TOS       ; -- f addr cnt USERSYS    [PSTACK-2] as USERSYS is cleared by ABORT
; ----------------------------------;
            CALL &ABORT_APP         ;
; ----------------------------------;
            CALL #INIT_SYS          ; -- f addr cnt USERSYS    common ?ABORT|PUC subroutine
            .word   DROP            ; -- f addr cnt
            .word   DUP             ; -- f addr cnt cnt
            .word   QFBRAN,ABORT_NXT; -- f addr 0       no display if ABORT" is an empty string
            .word   ECHO            ;                   force ECHO
            .word   XSQUOTE         ;
            .byte   6,CR,LF,ESC,"[7m";
            .word   TYPE            ;                   ESC [7m = set reverse video
; ----------------------------------;
; Display QABORT|WARM message       ; -- f addr cnt     <== WARM jumps here
; ----------------------------------;
ABORT_TYPE  .word   TYPE            ; -- f              type QABORT|WARM message
SDABORT_END .word   XSQUOTE         ;                   set normal video Display then goto ABORT
            .byte   6,CR,LF,ESC,"[0m";
            .word   TYPE            ;                   ESC [0m = set normal video
ABORT_NXT   .word   ABORT_DOWNLOAD  ; -- f              no return, param stack is cleared, also return stack.
; ----------------------------------;

;-------------------------------------------------------------------------------
; INIT TERMinal then enable I/O     ;
;-------------------------------------------------------------------------------

; ==================================;
INIT_HARD                           ; INIT value of HARD_APP called by WARM
; ==================================;
    MOV #0081h,&TERM_CTLW0          ; 8 bits, UC SWRST + UCLK = SMCLK, max 6MBds @24MHz
    MOV &TERMBRW_RST,&TERM_BRW      ; init value from FRAM INFO
    MOV &TERMMCTLW_RST,&TERM_MCTLW  ; init value from FRAM INFO
    BIS.B #BUS_TERM,&TERM_SEL       ; Configure pins TERM_UART
    BIC #1,&TERM_CTLW0              ; release UC_TERM from reset...
    BIS #WAKE_UP,&TERM_IE           ; enable interrupt for wake up on terminal input
    BIC #LOCKLPM5,&PM5CTL0          ; activate all previous I/O settings.
; ==================================;
INIT_STOP                           ; SOFT_APP default value
; ==================================;
TX_IDLE     BIT #1,&TERM_STATW      ;3 uart busy ?
            JNZ TX_IDLE             ;2 loop back while TERM_UART is busy
; ==================================;
INIT_SOFT                           ; SOFT_APP default value
; ==================================;
INIT_ABORT                          ; ABORT_APP default value
; ==================================;
            MOV @RSP+,PC            ; RET
; ----------------------------------;

;-----------------------------------;
UART_WARM                           ;
;-----------------------------------;
WARM    CALL &HARD_APP              ;
;-----------------------------------;
        mLO2HI                      ;
        .word   ECHO                ;
        .word   XSQUOTE             ;
        .byte   7,CR,LF,ESC,"[7m#"  ; CR+LF + cmd "reverse video" + '#'
        .word   TYPE                ;
        .WORD   DOT                 ; display TOS = USERSYS value
        .word   XSQUOTE             ;
        .byte   15,"FastForth V4.2 ";
        .word   TYPE                ;
        .word   LIT,FRAM_FULL       ;
        .word   HERE,MINUS,UDOT     ;
        .word   XSQUOTE             ;
        .byte   10,"bytes free"     ;
        .word   BRAN,ABORT_TYPE     ; no return
;-----------------------------------;

;-------------------------------------------------------------------------------
; INTERPRETER INPUT: ACCEPT KEY EMIT ECHO NOECHO
;-------------------------------------------------------------------------------

            FORTHWORD "ACCEPT"      ;
;-----------------------------------;
;https://forth-standard.org/standard/core/ACCEPT
;C ACCEPT  addr addr len -- addr len'  get up to len chars at addr
ACCEPT      MOV @PC+,PC             ;3 Code Field Address (CFA) of ACCEPT
PFAACCEPT   .word   BODYACCEPT      ;  Parameter Field Address (PFA) of ACCEPT
; ----------------------------------;
; ACCEPT part I prepare TERMINAL_INT;               this version allows to RX one char (LF) after sending XOFF
; ----------------------------------;
BODYACCEPT  MOV TOS,Y               ;1 -- org len   Y = len
            MOV @PSP,TOS            ;2 -- org ptr
            ADD TOS,Y               ;1 -- org ptr   Y = buf_end
            MOV #CR,X               ;2              X = 'CR' to speed up char loop in TERMINAL_INT part
            MOV #BL,W               ;2              W = 'BL' to speed up char loop in TERMINAL_INT part
            MOV #YEMIT_NEXT,T       ;2              T = YEMIT_ret
            MOV #CR_NEXT,S          ;2              S = XOFF_ret
            PUSHM #6,IP             ;8              PUSHM IP,S,T,W,X,Y       r-- ACCEPT_ret XOFF_ret YEMIT_ret BL CR buf_end

;###################################################################################
BACKGRND    CALL &BACK_APP          ;   default BACKGRND_APP = UART_INIT_BACKGRND = UART_RXON, value set by DEEP_RESET.
            BIS #LPM0+GIE,SR        ;2  enter in LPM0 mode with GIE=1
; here, FAST FORTH sleeps, awaiting any interrupt.
; IP,S,T,W,X,Y registers (R13 to R8) are free...
; ...and also TOS, PSP and RSP stacks within their rules of use.
            JMP BACKGRND            ;2  return for all interrupts.
;###################################################################################

; ----------------------------------; RXOFF is sent while LF char is received...
UART_RXOFF                          ; Software|hardware flow control to stop RX UART
; ----------------------------------; RXOFF is sent while LF char is received...
    .IFDEF TERMINAL3WIRES           ;   first software flow control
RXOFF_LOOP  BIT #TX_TERM,&TERM_IFG  ;3      wait the sending of last char
            JZ RXOFF_LOOP           ;2
            MOV #XOFF,&TERM_TXBUF   ;4      move XOFF char into TX_buf
    .ENDIF                          ;
    .IFDEF TERMINAL4WIRES           ;   then hardware flow control
            BIS.B #RTS,&HANDSHAKOUT ;3  set RTS high
    .ENDIF                          ;
            MOV @RSP+,PC            ;4 to CR_NEXT
; ----------------------------------; RXOFF is sent while LF char is received...

; **********************************;
TERMINAL_INT                        ; <--- TERM RX interrupt vector, delayed by the LPM0 wake up time
; **********************************;      if wake up time increases, max baudrate decreases...
; ACCEPT part II under interrupt    ; Org Ptr -- len'       all SR flags are cleared
; ----------------------------------;
            ADD #4,RSP              ;1  remove SR and PC from stack
            POPM #4,IP              ;6  W=BUF_end, T='CR', S='BL', IP=YEMIT_ret
; ----------------------------------;   r-- ACCEPT_ret XOFF_ret
AKEYREAD    MOV.B &TERM_RXBUF,Y     ;3  read character into Y, RX_TERM is cleared
; ----------------------------------;
            CMP.B S,Y               ;1      printable char ?
            JC ASTORETEST           ;2      yes
; ----------------------------------;
            CMP.B T,Y               ;1      CR ?
            JZ UART_RXOFF           ;2      yes    r-- ACCEPT_ret XOFF_ret 
; ----------------------------------;
            CMP.B #8,Y              ;1      char = BS ?
            JNZ WAITaKEY            ;2      case of other control chars
; ----------------------------------;
; start of backspace                ;
; ----------------------------------;
            CMP @PSP,TOS            ;       Ptr = Org ?
            JZ WAITaKEY             ;       yes: do nothing, loop back
            SUB #1,TOS              ;       no : dec Ptr
            JMP YEMIT               ;       don't store BS
; ----------------------------------;
; end of backspace                  ;
; ----------------------------------;
ASTORETEST  CMP W,TOS               ; 1 Bound is reached ?
            JC YEMIT                ; 2 yes: don't store char @ Ptr, don't increment TOS
            MOV.B Y,0(TOS)          ; 3 no: store char @ Ptr
            ADD #1,TOS              ; 1     increment Ptr
; ----------------------------------;
YEMIT       BIT #TX_TERM,&TERM_IFG  ; 3 NOECHO stores here : MOV @IP+,PC, ECHO stores here the first word of: BIT #TX_TERM,&TERM_IFG
            JZ YEMIT                ; 2 but there's no point in wanting to save time here:
        .IFDEF  TERMINAL5WIRES      ; then decrease BAUDRATE !
YEMITH      BIT.B #CTS,&HANDSHAKIN  ; 3 CTS is pulled low if unwired.
            JNZ YEMITH              ; 2
        .ENDIF                      ;
            MOV.B Y,&TERM_TXBUF     ; 3
            MOV @IP+,PC             ; 4
; ----------------------------------;
YEMIT_NEXT  mHI2LO                  ; 0 YEMIT NEXT address
            SUB #2,IP               ; 1 restore YEMIT_NEXT
; ----------------------------------;
WAITaKEY    BIT #RX_TERM,&TERM_IFG  ; 3 new char in TERMRXBUF ?
            JNZ AKEYREAD            ; 2 yes, loop = 34~/26~ by char (with/without echo) ==> 294/384 kBds per MHz
            JMP WAITaKEY            ; 2 no
; ----------------------------------;
; return of RXOFF                   ; --- Org Ptr   r-- ACCEPT_ret
; ----------------------------------;
CR_NEXT     BIT #RX_TERM,&TERM_IFG  ;               char 'LF' is received ?
            JZ CR_NEXT              ;               no
            MOV.B &TERM_RXBUF,Y     ;               yes, TERM_INT must be cleared when we quit ACCEPT
; ----------------------------------;
            SUB @PSP+,TOS           ; -- len'       R-- ACCEPT_NEXT
            MOV @RSP+,IP            ; r--
            MOV.B S,Y               ; output a BL on TERMINAL (for the case of error occuring)
            JMP YEMIT               ; before return to QUIT to interpret line
; **********************************;

            FORTHWORD "KEY"
;-----------------------------------;
; https://forth-standard.org/standard/core/KEY
; KEY      -- c      wait character from input device ; primary DEFERred word
KEY         MOV @PC+,PC             ;4  Code Field Address (CFA) of KEY
PFAKEY      .word   BODYKEY         ;   Parameter Field Address (PFA) of KEY, with default value
BODYKEY     PUSH #KEYNEXT           ;
; ==================================;
INIT_BACK                           ; INIT value of BACK_APP
; ==================================;
UART_RXON                           ; = KEY + $08
; ----------------------------------;
    .IFDEF TERMINAL3WIRES           ;   first software flow control
            BIT #TX_TERM,&TERM_IFG  ;3      wait the sending of last char, useless at high baudrates
            JZ UART_RXON            ;2
            MOV #XON,&TERM_TXBUF    ;4  move char XON into TX_buf
    .ENDIF                          ;
    .IFDEF TERMINAL4WIRES           ;   and hardware flow control after
            BIC.B #RTS,&HANDSHAKOUT ;3      set RTS low
    .ENDIF                          ;
            MOV @RSP+,PC            ;4
; ----------------------------------;
KEYNEXT     SUB #2,PSP              ;1  push old TOS..
            MOV TOS,0(PSP)          ;3  ..onto stack
KEYLOOP     BIT #RX_TERM,&TERM_IFG  ; loop if bit0 = 0 in interupt flag register
            JZ KEYLOOP              ;
            CALL #UART_RXOFF        ;
            MOV &TERM_RXBUF,TOS     ;
            MOV @IP+,PC
;-----------------------------------;

            FORTHWORD "EMIT"        ;       save X before use if used
;-----------------------------------;
; https://forth-standard.org/standard/core/EMIT
; EMIT     c --    output character to the selected output device ; primary DEFERred word
EMIT        MOV @PC+,PC             ;3 Code Field Address (CFA) of EMIT
PFAEMIT     .word   BODYEMIT        ;  Parameter Field Address (PFA) of EMIT, with its default value
BODYEMIT    MOV TOS,Y               ;1 output character to the default output: TERMINAL
            MOV @PSP+,TOS           ;2
            JMP YEMIT               ;2 + 17~/4~
;-----------------------------------;

            FORTHWORD "ECHO"        ; --    connect ACCEPT and EMIT to TERMINAL input (default)
;-----------------------------------;
ECHO        MOV #0B3A2h,&YEMIT      ;       opcode of "BIT #TX_TERM,&YEMIT" --> YEMIT
            MOV @IP+,PC             ;
;-----------------------------------;

            FORTHWORD "NOECHO"      ; --    disconnect ACCEPT and EMIT from TERMINAL input
;-----------------------------------;
NOECHO      MOV #4D30h,&YEMIT       ;       opcode @IP+,PC --> YEMIT adr
            MOV @IP+,PC             ;
;-----------------------------------;
